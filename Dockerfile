FROM    nginx:alpine
ENV PORT=8080
COPY    . /etc/nginx/
RUN ln -sf /dev/stdout /var/log/nginx/bots.access.log
CMD ["/bin/sh", "-c", "exec nginx -g 'daemon off;';"]
